import requests, json
import os
from datetime import datetime


currentPath = os.getcwd() + '/call_api/'


def call_api(prd_cd, date):
    # 키 입력 필수
    # key = 
    
    # date = '20210311'
    # prd_cd = '1201'
    req_cnt = '2000'
    url = f'http://apis.data.go.kr/B552895/openapi/service/OrgPriceAuctionService/getExactProdPriceList?ServiceKey={key}&pageNo=1&numOfRows={req_cnt}&delngDe={date}&prdlstCd={prd_cd}&_type=json'

    res = requests.get(url)
    jo = json.loads(res.content)
    body = jo['response']['body']
    item = body['items']['item']
    print(f'date:{date} len:{len(item)} body:{body["totalCount"]}')

    file_name = currentPath + f'res_json/{date}_{prd_cd}.json'
    with open(file_name, 'w+') as f:
        f.write(json.dumps(item))
        print(file_name, 'save success')

dates = ['20210302', '20210303', '20210304', '20210305', '20210308', '20210311', '20210312', '20210313']
for date in dates:
    print(date, 'start:', datetime.now())
    call_api('1202', date)
    print(date, 'end: ', datetime.now())
    print("---------")